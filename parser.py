#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from lxml.html import fromstring
from datetime import datetime
import requests
import csv, re, os

path = os.getcwd()
inputfile = path + '/' + 'in.csv'
outputfile = path + '/' + 'out.csv'

def makeheader():
    out = []
    out.append('url')
    out.append('region')
    out.append('placement_type')
    out.append('brand')
    out.append('cement_type')
    out.append('weight')
    out.append('strenght')
    out.append('title')
    out.append('price')
    out.append('date')
    return out

def gethtml(url): # общая функция запроса страницы
    session = requests.session()
    if("http://" in url):
        resp = session.get(url)
    else:
        resp = session.get('http://' + url)
    html = fromstring(resp.content)
    return html

def gethtmlutf8(url): # общая функция запроса страницы с кодировкой utf-8, когда она не определена явно
    session = requests.session()
    if("http://" in url):
        resp = session.get(url)
    else:
        resp = session.get('http://' + url)
    html = fromstring(resp.content.decode('utf-8'))
    return html

def rewrite(row):
    outrow = []
    outrow.append(row[0]) # url
    outrow.append(row[1]) # region
    outrow.append(row[2]) # pacement_type
    outrow.append(row[3])  # brand
    outrow.append(row[4])  # cement_type
    outrow.append(row[5])  # weight
    outrow.append(row[6])  # strength
    return outrow


def leroymerlin(row):  # выборка данных leroymerlin
    out = []
    html = gethtml(row[0])
    out.append(row[0])
    out.append(row[1])
    out.append(row[2])
    brand = None
    cement_type = None
    weight = None
    strenght = None
    title = None
    price_all = None
    try:
        title = html.xpath(".//*[@class='product__hl']")[0].text
        price = html.xpath(".//*[@class='product__col']/p")[1]
        price_cop = html.xpath(".//*[@class='product__col']/p/span")[0]
        haracteristics = html.xpath(".//*[@class='about__params__table']/tbody/tr/td")
        is_brand = False
        for har in haracteristics:
            text = re.findall(r'\w+', har.text)
            if is_brand == True:
                is_brand = False
                brand = text[0]
            if ((text[0].lower() == 'марка') and (len(text)<=1) ):
                is_brand=True
        price_all = re.sub(r'\s+', '',re.findall(r'\d+', price.text)[0] + price_cop.text)
        result = re.findall(r'\w+', title)
        cement_type = result[0]
        weight = result[2]
        strenght = re.findall(r'\d+', result[1])[0]
    except:
        pass

    out.append(brand)
    out.append(cement_type)
    out.append(weight)
    out.append(strenght)
    out.append(title)
    out.append(price_all)
    out.append(datetime.today())
    return out

def stroymarket(row):   # выборка данных Строймаркет
    out = []
    html = gethtml(row[0])
    out.append(row[0])
    out.append(row[1])
    out.append(row[2])
    brand = None
    cement_type = None
    weight = None
    strenght = None
    title = None
    price = None
    try:
        price = re.sub(r'\s+', '',html.xpath(".//*[@class='custom-item-price']")[0].text)
        title = html.xpath(".//*[@class='product-name']")[0].text
        haracteristics = html.xpath(".//*[@class='dl-inline-attribute dl-product-attribute']/dd")
        weight = haracteristics[1].text
        brand = haracteristics[2].text
        strenght = re.findall(r'\d+', haracteristics[3].text)[0]
        cement_type = re.findall(r'\w+', title)[0]
    except:
        pass
    out.append(brand)
    out.append(cement_type)
    out.append(weight)
    out.append(strenght)
    out.append(title)
    out.append(price)
    out.append(datetime.today())
    return out

def castorama(row):
    out = []
    html = gethtml(row[0])
    out.append(row[0])
    out.append(row[1])
    out.append(row[2])
    brand = None
    cement_type = None
    weight = None
    strenght = None
    title = None
    price = None
    try:
        haracteristics = html.xpath(".//*[@id='product-attribute-specs-table']/dd")
        cement_type = re.findall(r'\w+',haracteristics[0].text)[0]
        strenght = re.findall(r'\d+',haracteristics[2].text)[0]
        weight = re.findall(r'\d+',haracteristics[5].text)[0]
        brand = re.findall(r'\w+',html.xpath(".//*[@id='product-attribute-specs-table']/dd/span")[0].text)[0]
        title = html.xpath(".//*[@id='review-form']/h3/span")[0].text
        price = re.sub(r'\s+', '', html.xpath(".//*[@class='price']/span/span")[0].text)
    except:
        pass
    out.append(brand)
    out.append(cement_type)
    out.append(weight)
    out.append(strenght)
    out.append(title)
    out.append(price)
    out.append(datetime.today())
    return out

def sm62(row):
    out = []
    html = gethtmlutf8(row[0])
    out.append(row[0])
    out.append(row[1])
    out.append(row[2])
    brand = None
    cement_type = None
    weight = None
    strenght = None
    title = None
    price = None
    try:
        title = re.sub(r'\s+', ' ', html.xpath(".//*[@class='title']/h1/text()")[0])
        cement_type = re.findall(r'\w+', title)[0]
        weight = re.findall(r'(\d\d)\w\w', title)[0]
        strenght = re.findall(r'\d\d\d', title)[0]
    except:
        pass
    try:
        price = re.findall(r'\d+\,\d+', html.xpath(".//*[@class='product-price']")[0].text)[0]
    except:
        pass
    out.append(brand)
    out.append(cement_type)
    out.append(weight)
    out.append(strenght)
    out.append(title)
    out.append(price)
    out.append(datetime.today())
    return out

def rusgvozdi(row):
    out = []
    html = gethtml(row[0])
    out.append(row[0])
    out.append(row[1])
    out.append(row[2])
    brand = None
    cement_type = None
    weight = None
    strenght = None
    title = None
    price = None
    try:
        title = html.xpath(".//*[@class='col-md-8']/h1")[0].text
        cement_type = re.findall(r'\w+', title)[0]
        weight = re.findall(r'(\d\d)\w\w', title)[0]
        strenght = re.findall(r'\w-(\d+)', title)[0]
        brand = re.findall(r'\w+',title)[5] + " " + re.findall(r'\w+',title)[6]
        price =  re.findall(r'\d+\.\d+',  html.xpath(".//*[@class='price']")[0].text )[0]
    except:
        pass
    out.append(brand)
    out.append(cement_type)
    out.append(weight)
    out.append(strenght)
    out.append(title)
    out.append(price)
    out.append(datetime.today())
    return out

def bazavelikan(row):
    out = []
    html = gethtml(row[0])
    out.append(row[0])
    out.append(row[1])
    out.append(row[2])
    brand = None
    cement_type = None
    weight = None
    strenght = None
    title = None
    price = None
    try:
        title = html.xpath(".//*[@class='company-header-title']")[0].text
        cement_type = re.findall(r'\w+',title)[0]
        strenght = re.findall(r'\d+',title)[0]
        price = re.sub(r'\s+', '',html.xpath(".//*[@class='bp-price fsn']/text()")[0])
    except:
        pass
    out.append(brand)
    out.append(cement_type)
    out.append(weight)
    out.append(strenght)
    out.append(title)
    out.append(price)
    out.append(datetime.today())
    return out

def simarket(row):
    out = []
    html = gethtml(row[0])
    out.append(row[0])
    out.append(row[1])
    out.append(row[2])
    brand = None
    cement_type = None
    weight = None
    strenght = None
    title = None
    price = None
    try:
        title = html.xpath(".//*[@class='offer-name']/text()")[0]
        cement_type = re.findall(r'\w+', title)[0]
        strenght = re.findall(r'\w+-(\d\d\d)', title)[0]
        weight = re.findall(r'\d\d', title)[0]
        price = re.findall(r'\d+\.\d+', html.xpath(".//*[@class='col-md-3 text-center prodprice cost1 actual_price']/text()")[0])[0]
    except:
        pass
    out.append(brand)
    out.append(cement_type)
    out.append(weight)
    out.append(strenght)
    out.append(title)
    out.append(price)
    out.append(datetime.today())
    return out
    pass

def udachniykazan(row):
    out = []
    html = gethtml(row[0])
    out.append(row[0])
    out.append(row[1])
    out.append(row[2])
    brand = None
    cement_type = None
    weight = None
    strenght = None
    title = None
    price = None
    try:
        title = html.xpath(".//*[@class='content-inner']/h1/text()")[0]
        price = re.sub(r'\s+', '', html.xpath(".//*[@class='product-price']/span/text()")[0])
        cement_type = re.findall(r'\w+', title)[0]
        strenght = re.findall(r'\w+-(\d\d\d)', title)[0]
    except:
        pass
    out.append(brand)
    out.append(cement_type)
    out.append(weight)
    out.append(strenght)
    out.append(title)
    out.append(price)
    out.append(datetime.today())
    return out

def megastroy(row):
    out = []
    html = gethtml(row[0])
    out.append(row[0])
    out.append(row[1])
    out.append(row[2])
    brand = None
    cement_type = None
    weight = None
    strenght = None
    title = None
    price = None
    try:
        price = re.sub(r'\s+', '', html.xpath(".//*[@class='item_current_price']/span/text()")[0])
        title = re.sub(r'\s+', ' ', html.xpath(".//*[@id='content']/h1/text()")[0])
        cement_type = re.findall(r'\w+', title)[0]
        weight = re.findall(r'(\d\d)\w+', title)[0]
        strenght = re.findall(r'\w+-(\d\d\d)', title)[0]
    except:
        pass
    out.append(brand)
    out.append(cement_type)
    out.append(weight)
    out.append(strenght)
    out.append(title)
    out.append(price)
    out.append(datetime.today())
    return out

def belydom(row):
    out = []
    html = gethtml(row[0])
    out.append(row[0])
    out.append(row[1])
    out.append(row[2])
    brand = None
    cement_type = None
    weight = None
    strenght = None
    title = None
    price = None
    try:
        price = re.findall(r'\d+', html.xpath(".//*[@class='price']/text()")[0])[0]
        title = re.sub(r'\s+', ' ', html.xpath(".//*[@class='catalog-element-name']/h1/text()")[0])
        cement_type = re.findall(r'\w+', title)[0]
        weight = re.findall(r'(\d\d)\w+', title)[0]
        brand = re.findall(r'\w+', title)[1]
    except:
        pass
    out.append(brand)
    out.append(cement_type)
    out.append(weight)
    out.append(strenght)
    out.append(title)
    out.append(price)
    out.append(datetime.today())
    return out

def stroydepo(row):
    out = []
    html = gethtml(row[0])
    out.append(row[0])
    out.append(row[1])
    out.append(row[2])
    brand = None
    cement_type = None
    weight = None
    strenght = None
    title = None
    price = None
    try:
        #price = re.findall(r'\d+', html.xpath(".//*[@class='price']/text()")[0])[0]
        title = re.sub(r'\s+', ' ', html.xpath(".//*[@class='element-name']/text()")[0])
        cement_type = re.findall(r'\w+', title)[0]
        weight = re.findall(r'(\d\d) кг', title)[0]
        #brand = re.findall(r'\w+', title)[1]
    except:
        pass
    out.append(brand)
    out.append(cement_type)
    out.append(weight)
    out.append(strenght)
    out.append(title)
    out.append(price)
    out.append(datetime.today())
    return out

def profcom64(row):
    out = []
    html = gethtml(row[0])
    out.append(row[0])
    out.append(row[1])
    out.append(row[2])
    brand = None
    cement_type = None
    weight = None
    strenght = None
    title = None
    price = None
    try:
        price = re.findall(r'\d+', html.xpath(".//*[@class='price']/span/text()")[0])[0]
        title = re.sub(r'\s+', ' ', html.xpath(".//*[@class='h']/h1/text()")[0])
        cement_type = re.findall(r'\w+', title)[0]
        weight = re.findall(r'(\d\d)[\s\w]', title)[0]
        strenght = re.findall(r'\d\d\d', title)[0]
        #brand = re.findall(r'\w+', title)[1]
    except:
        pass
    print(title)
    print(weight)
    out.append(brand)
    out.append(cement_type)
    out.append(weight)
    out.append(strenght)
    out.append(title)
    out.append(price)
    out.append(datetime.today())
    return out

def bsm64(row):
    out = []
    html = gethtml(row[0])
    out.append(row[0])
    out.append(row[1])
    out.append(row[2])
    brand = None
    cement_type = None
    weight = None
    strenght = None
    title = None
    price = None
    try:
        price = re.findall(r'\d+', html.xpath(".//*[@class='price']/strong/text()")[0])[0]
        title = re.sub(r'\s+', ' ', html.xpath(".//*[@class='inner']/h1/text()")[0])
        cement_type = re.findall(r'\w+', title)[0]
        weight = re.findall(r'(\d\d)[\s\w]', title)[0]
        strenght = re.findall(r'\d\d\d', title)[0]
        #brand = re.findall(r'\w+', title)[1]
    except:
        pass
    out.append(brand)
    out.append(cement_type)
    out.append(weight)
    out.append(strenght)
    out.append(title)
    out.append(price)
    out.append(datetime.today())
    return out


def stroydvor2002(row):
    out = []
    html = gethtml(row[0])
    out.append(row[0])
    out.append(row[1])
    out.append(row[2])
    brand = None
    cement_type = None
    weight = None
    strenght = None
    title = None
    price = None
    try:
        price = re.findall(r'\d+', html.xpath(".//*[@id='block_price']/text()")[0])[0]
        title = re.sub(r'\s+', ' ', html.xpath(".//*[@class='jshop productfull']/form/h1/text()")[0])
        cement_type = re.findall(r'\w+', title)[0]
        weight = re.findall(r'(\d\d)[\s\w]', title)[0]
        strenght = re.findall(r'\d\d\d', title)[0]
        # brand = re.findall(r'\w+', title)[1]
    except:
        pass
    out.append(brand)
    out.append(cement_type)
    out.append(weight)
    out.append(strenght)
    out.append(title)
    out.append(price)
    out.append(datetime.today())
    return out


def stroykatd(row):
    out = []
    html = gethtml(row[0])
    out.append(row[0])
    out.append(row[1])
    out.append(row[2])
    brand = None
    cement_type = None
    weight = None
    strenght = None
    title = None
    price = None
    try:
        title = re.sub(r'\s+', ' ', html.xpath(".//*[@class='column_wrap']/article/h1/text()")[0])
        cement_type = re.findall(r'\w+', title)[0]
        weight = re.findall(r'(\d\d)[\s\w]', title)[0]
        strenght = re.findall(r'\d\d\d', title)[0]
        # brand = re.findall(r'\w+', title)[1]
        price =  html.xpath(".//*[@class='price-current']/strong/text()")[0]
    except:
        pass
    out.append(brand)
    out.append(cement_type)
    out.append(weight)
    out.append(strenght)
    out.append(title)
    out.append(price)
    out.append(datetime.today())
    return out


def rusdom(row):
    out = []
    html = gethtml(row[0])
    out.append(row[0])
    out.append(row[1])
    out.append(row[2])
    brand = None
    cement_type = None
    weight = None
    strenght = None
    title = None
    price = None
    try:
        title = re.sub(r'\s+', ' ', html.xpath(".//*[@class='bx_item_title']/h1/span/text()")[0])
        cement_type = re.findall(r'\w+', title)[0]
        weight = re.findall(r'(\d\d)[\s\w]', title)[0]
        strenght = re.findall(r'\d\d\d', title)[0]
    except:
        pass
    try:
        brand = re.findall(r'"(\w+[\w\s]\w+)"', title)[0]
    except:
        pass
    try:
        price = re.findall(r'\d+', html.xpath(".//*[@class='item_current_price']/text()")[0])[0]
    except:
        pass
    out.append(brand)
    out.append(cement_type)
    out.append(weight)
    out.append(strenght)
    out.append(title)
    out.append(price)
    out.append(datetime.today())
    return out


if __name__ == '__main__':
    csv_in =  csv.reader(open(inputfile),delimiter=',', quotechar='|')
    csv_out = csv.writer(open(outputfile, 'w+'))
    #csv_out.writerow(makeheader())
    next(csv_in)
    for row in csv_in:
        #print (row)
        if ("leroymerlin.ru" in row[0]):
            #csv_out.writerow(leroymerlin(row))
            pass
        elif("stroykamarket.su" in row[0]):
            #csv_out.writerow(stroymarket(row))
            pass
        elif("castorama.ru" in row[0]):
            #csv_out.writerow(castorama(row))
            pass
        elif("sm62.ru" in row[0]):
            #csv_out.writerow(sm62(row))
            pass
        elif("russkie-gvozdi.ru" in row[0]):
            #csv_out.writerow(rusgvozdi(row))
            pass
        elif ("xn--80aaabhsllsiz.xn--p1ai" in row[0]):
            #csv_out.writerow(bazavelikan(row))
            pass
        elif ("si-market.ru" in row[0]):
            #csv_out.writerow(simarket(row))
            pass
        elif ("udachnyikazan.ru" in row[0]):
            #csv_out.writerow(udachniykazan(row))
            pass
        elif ("megastroy.com" in row[0]):
            #csv_out.writerow(megastroy(row))
            pass
        elif ("belydom.ru" in row[0]):
            #csv_out.writerow(belydom(row))
            pass
        elif ("stroydepo.ru" in row[0]):
            #csv_out.writerow(stroydepo(row))
            pass
        elif ("profkom64.ru" in row[0]):
            #csv_out.writerow(profcom64(row))
            pass
        elif ("bsm64.ru" in row[0]):
            #csv_out.writerow(bsm64(row))
            pass
        elif ("stroydvor2002.ru" in row[0]):
            #csv_out.writerow(stroydvor2002(row))
            pass
        elif ("stroyka-td.ru" in row[0]):
            #csv_out.writerow(stroykatd(row))
            pass
        elif ("rusdom-tlt.ru" in row[0]):
            csv_out.writerow(rusdom(row))
            pass
